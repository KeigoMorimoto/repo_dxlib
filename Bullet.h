#ifndef INCLUDED_BULLET_H
#define INCLUDED_BULLET_H

#include "AnimDraw.h"
//#include "DynamicObject.h"
#include "macro.h"
//#include "StaticObject.h"

class Bullet {
public:


	enum Direction {
		LEFT,
		RIGHT,
		UP,
		DOWN,

		NULL_Direction
	};


	enum Player {
		oneP,
		twoP,

		NullP
	};


	Bullet(int mx, int my, int direction, int player);
	~Bullet();
	//void set(int mx, int my, Direction direction, Player player);
	void draw() const;
	bool updata();


	Direction mDirection;
	Player mPlayer;
	int mX;
	int mY;

	bool isIntersectWall(int wallX, int wallY);
	bool isIntersectWall(int x, int y, int wallX, int wallY);

	bool GotShot(int mx, int my);
	void getCell(int* x, int* y) const;

private:

	void getVelocity(int* dx, int* dy);
	void getDirection(int* dx, int* dy);
	

	int OneBulletNum;
	int TwoBulletNum;

	
};

#endif