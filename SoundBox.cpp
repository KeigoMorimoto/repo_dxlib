#include"SoundBox.h"

SoundBox* SoundBox::mSoundBoxInstance = 0;

map<string, int> SoundsMap;
map<string, int>::iterator it;

void SoundBox::create(){
	mSoundBoxInstance = new SoundBox();
}

void SoundBox::destroy() {
	SAFE_DELETE(mSoundBoxInstance);
}

SoundBox* SoundBox::instance() {
	return mSoundBoxInstance;
}

int SoundBox::set(int shandle) {
	sounds.push_back(shandle);
	return (sounds.size() - 1);
}

void SoundBox::play(int snum) {
	DrawFormatString(100, 100, GetColor(0, 255, 0), "Play"); // 文字を描画する
	PlaySoundMem(sounds.at(snum), DX_PLAYTYPE_BACK);
}

void SoundBox::stop(int snum) {
	StopSoundMem(sounds.at(snum));
}

void SoundBox::playbgm(int snum) {
	int newbgm = sounds.at(snum);
	if (bgm == newbgm) return;
	StopSoundMem(bgm);
	bgm = newbgm;
	PlaySoundMem(bgm, DX_PLAYTYPE_LOOP);
}

void SoundBox::SoundsMap_set(string name, int shandle) {
	SoundsMap.insert(make_pair(name, shandle));
}

void SoundBox::SoundsMap_play(string name) {
	it = SoundsMap.find(name);
	PlaySoundMem((*it).second, DX_PLAYTYPE_BACK);
}

void SoundBox::SoundsMap_stop(string name) {
	it = SoundsMap.find(name);
	StopSoundMem((*it).second);
}

void SoundBox::SoundsMap_playbgm(string name) {
	//string newbgm = name;
	/*
	if (map_bgm.compare(newbgm) == 1) {
		DrawFormatString(300, 10, GetColor(0, 255, 0), "exit"); // 文字を描画する
	}
	*/
	//
	//it = SoundsMap.find(name);
	//StopSoundMem((*it).second);
	//map_bgm = newbgm;
	

	//if (current_bgm == name) return;
	
	//it = SoundsMap.find(current_bgm);
	//StopSoundMem((*it).second);
	//DrawFormatString(0, 10, GetColor(255, 255, 0), "exit"); // 文字を描画する
	
	
	it = SoundsMap.find(name);
	PlaySoundMem((*it).second, DX_PLAYTYPE_LOOP);

	current_bgm = name;
}