#ifndef INCLUDED_BASE_H
#define INCLUDED_BASE_H

	class SceneMng;

	class Base {
	public:
		virtual ~Base() {} //なにもしないのでこれでいい
		virtual void update(SceneMng*) = 0;
	};


#endif