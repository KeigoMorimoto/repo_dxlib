#ifndef INCLUDED_GAME_H
#define INCLUDED_GAME_H

#include "iostream"
#include "Base.h"
#include "SceneMng.h"
#include "DxLib.h"
//#include "State.h"
#include "macro.h"

class SceneMng;
class State;

class Game : public Base {
public:
	Game();
	~Game();
	void update(SceneMng*);
	

private:
	//Image * mImage; //タイトル画面画像
	//int mCursorPosistion;
	State* mState;

	
};


#endif