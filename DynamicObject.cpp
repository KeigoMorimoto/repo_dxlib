#include "DynamicObject.h"


//速度,単位は内部単位/フレーム
static const int PLAYER_SPEED = 2000;
//static const int ENEMY_SPEED = 500;
static const int HALF_SIZE = 8000;

//内部単位
static int convertCellToInner(int x) {
	return x * 32000 + 16000;
}
//内部単位から画素単位
static int convertInnerToPixel(int x) {
	return  (x + 500 - 16000) / 1000;
}

//できるだけ不正くさい値を入れておく。setが呼ばれないと死ぬように。
DynamicObject::DynamicObject() :
	mType(TYPE_NONE),
	mPlayerID(0xffffffff),
	mX(-1),
	mY(-1),
	ButtonCount(0)
{
}

void DynamicObject::set(int x, int y, Type type) {

	mX = convertCellToInner(x);
	mY = convertCellToInner(y);

	mType = type;
}


void DynamicObject::draw() const {

	int dstX = convertInnerToPixel(mX);
	int dstY = convertInnerToPixel(mY);

	if (mType == TYPE_PLAYER) {

		if (Current_Anim_ID == Anim_ID_STOP || Current_Anim_ID == Anim_ID_STOP_DOWN) {
			AnimDraw::instance()->draw(0, dstX, dstY);
		}
		else if (Current_Anim_ID == Anim_ID_STOP_UP) {
			AnimDraw::instance()->draw(7, dstX, dstY);
		}
		else if (Current_Anim_ID == Anim_ID_STOP_LEFT) {
			AnimDraw::instance()->draw(5, dstX, dstY);
		}
		else if (Current_Anim_ID == Anim_ID_STOP_RIGHT) {
			AnimDraw::instance()->draw(6, dstX, dstY);
		}
		else if (Current_Anim_ID == Anim_ID_LEFT) {
			AnimDraw::instance()->draw(1, dstX, dstY);
		}
		else if (Current_Anim_ID == Anim_ID_RIGHT) {
			AnimDraw::instance()->draw(2, dstX, dstY);
		}
		else if (Current_Anim_ID == Anim_ID_UP) {
			AnimDraw::instance()->draw(3, dstX, dstY);
		}
		else if (Current_Anim_ID == Anim_ID_DOWN) {
			AnimDraw::instance()->draw(4, dstX, dstY);
		}

		if (mWD == WEAPON_RIGHT) {
			AnimDraw::instance()->draw(21, dstX + 32, dstY);
		}
		else if (mWD == WEAPON_LEFT) {
			AnimDraw::instance()->draw(22, dstX - 32, dstY);
		}
		else if (mWD == WEAPON_UP) {
			AnimDraw::instance()->draw(23, dstX, dstY - 32);
		}
		else if (mWD == WEAPON_DOWN) {
			AnimDraw::instance()->draw(24, dstX, dstY + 32);
		}
	}
}

void DynamicObject::move(int *wallsX, int *wallsY, int wallNumber) {

	int dx, dy;

	getVelocity(&dx, &dy);
	
	int movedX = mX + dx;
	int movedY = mY + dy;

	bool hitX = false;
	bool hitY = false;
	bool hit = false;

	for (int i = 0; i < wallNumber; i++) {
		if (isIntersectWall(movedX, mY, wallsX[i], wallsY[i])) {
			hitX = hit = true;
		}
		if (isIntersectWall(mX, movedY, wallsX[i], wallsY[i])) {
			hitY = hit = true;
		}
	}
	if (hitX && !hitY) {
		mY = movedY; //Yのみ
	}
	else if (!hitX && hitY) {
		mX = movedX; //Xのみ
	}
	else { //X,Y衝突
		for (int i = 0; i < wallNumber; i++) {
			if (isIntersectWall(movedX, movedY, wallsX[i], wallsY[i])) {
				hit = true;
			}
		}
		if (!hit) {
			mX = movedX;
			mY = movedY;
		}
	}

	if (mX > 768000) {
		mX = 32000;
	}
	else if (mX < 32000) {
		mX = 768000;
	}
	else if (mY < 32000) {
		mY = 544000;
	}
	else if (mY > 544000) {
		mY = 32000;
	}

	//mX = movedX;
	//mY = movedY;
}

/*

void DynamicObject::move(GLFWwindow *window, int* wallsX, int *wallsY, int wallNumber) {
srand((unsigned int)time(NULL));

//敵挙動
if (mType == TYPE_ENEMY) {
//単位を追ってみよう。
//mDirectionXは単位なし。dtはミリ秒。ENEMY_SPEEDは画素/秒。
//この段階ではミリ秒*画素/秒
//分母に1000をかけると分母と分子がミリ秒になって相殺。
//画素を内部座標に変えるには1000倍する必要がある。分子と分母で1000が相殺。
//以上から以下のようになる。
mX += mDirectionX * ENEMY_SPEED;
mY += mDirectionY * ENEMY_SPEED;
}
else if (mType == TYPE_1P) { //プレイヤー挙動
int dx, dy;
dx = dy = 0;

if (glfwGetKey(window, GLFW_KEY_W) != GLFW_RELEASE) {
dy = -1;
}
else if (glfwGetKey(window, GLFW_KEY_S) != GLFW_RELEASE) {
dy = 1;
}
else if (glfwGetKey(window, GLFW_KEY_A) != GLFW_RELEASE) {
dx = -1;
}
else if (glfwGetKey(window, GLFW_KEY_D) != GLFW_RELEASE) {
dx = 1;
}

mX += dx * PLAYER_SPEED;
mY += dy * PLAYER_SPEED;
}
else if (mType == TYPE_2P) { //プレイヤー挙動
int dx, dy;
dx = dy = 0;

if (glfwGetKey(window, GLFW_KEY_UP) != GLFW_RELEASE) {
dy = -1;
}
else if (glfwGetKey(window, GLFW_KEY_DOWN) != GLFW_RELEASE) {
dy = 1;
}
else if (glfwGetKey(window, GLFW_KEY_LEFT) != GLFW_RELEASE) {
dx = -1;
}
else if (glfwGetKey(window, GLFW_KEY_RIGHT) != GLFW_RELEASE) {
dx = 1;
}

mX += dx * PLAYER_SPEED;
mY += dy * PLAYER_SPEED;
}
int dx, dy;

getVelocity(&dx, &dy, window);

//mX += dx;
//mY += dy;

//X,Y別々に移動した時に当たるかチェック
int movedX = mX + dx;
int movedY = mY + dy;
bool hitX = false;
bool hitY = false;
bool hit = false;

for (int i = 0; i < wallNumber; ++i) {
if (isIntersectWall(movedX, mY, wallsX[i], wallsY[i])) {
hitX = hit = true;
}
if (isIntersectWall(mX, movedY, wallsX[i], wallsY[i])) {
hitY = hit = true;
}
}
if (hitX && !hitY) {
mY = movedY; //Yのみオーケー
}
else if (!hitX && hitY) {
mX = movedX; //Xのみオーケー
}
else { //ダメなので普通に
for (int i = 0; i < wallNumber; ++i) {
if (isIntersectWall(movedX, movedY, wallsX[i], wallsY[i])) {
hit = true;
}
}
if (!hit) {
mX = movedX;
mY = movedY;
}
}


/*
//限界処理
const int X_MIN = 24000;
const int X_MAX = 320 * 1000 - 40000;
const int Y_MIN = 24000;
const int Y_MAX = 240 * 1000 - 24000;
bool hit = false;
if (mX < X_MIN) {
mX = X_MIN;
hit = true;
}
else if (mX > X_MAX) {
mX = X_MAX;
hit = true;
}
if (mY < Y_MIN) {
mY = Y_MIN;
hit = true;
}
else if (mY > Y_MAX) {
mY = Y_MAX;
hit = true;
}



//敵なら向き変え
if (hit && mType == TYPE_ENEMY) {
mDirectionX = mDirectionY = 0;
switch (rand() % 4) {
case 0: mDirectionX = 1; break;
case 1: mDirectionX = -1; break;
case 2: mDirectionY = 1; break;
case 3: mDirectionY = -1; break;
}
}

}

*/


void DynamicObject::getVelocity(int* dx, int* dy)  {
//スピードを変数に格納
int speedX, speedY;
/*
if (mType == TYPE_ENEMY) {
speedX = ENEMY_SPEED;
speedY = ENEMY_SPEED;
}
*/

speedX = PLAYER_SPEED;
speedY = PLAYER_SPEED;

//向き取得
getDirection(dx, dy);
//速度計算
*dx = *dx * speedX;
*dy = *dy * speedY;


}


void DynamicObject::getDirection(int* dx, int* dy)  {

	*dx = *dy = 0;


	if (mType == TYPE_PLAYER) {

		Current_Anim_ID = AnimID::Anim_ID_STOP;

		//1P
		if (mPlayerID == 0) {
			if (CheckHitKey(KEY_INPUT_A) == 1) {
				Current_Anim_ID = AnimID::Anim_ID_LEFT;
				Current_Direction = Direction::LEFT;
				*dx = -1;
			}

			if (CheckHitKey(KEY_INPUT_D) == 1) {
				Current_Anim_ID = AnimID::Anim_ID_RIGHT;
				Current_Direction = Direction::RIGHT;
				*dx = 1;
			}

			if (CheckHitKey(KEY_INPUT_W) == 1) {
				Current_Anim_ID = AnimID::Anim_ID_UP;
				Current_Direction = Direction::UP;
				*dy = -1;
			}

			if (CheckHitKey(KEY_INPUT_S) == 1) {
				Current_Anim_ID = AnimID::Anim_ID_DOWN;
				Current_Direction = Direction::DOWN;
				*dy = 1;
			}
		}

		//2P
		if (mPlayerID == 1) {
			if (CheckHitKey(KEY_INPUT_LEFT) == 1) {
				Current_Anim_ID = AnimID::Anim_ID_LEFT;
				Current_Direction = Direction::LEFT;
				*dx = -1;
			}

			if (CheckHitKey(KEY_INPUT_RIGHT) == 1) {
				Current_Anim_ID = AnimID::Anim_ID_RIGHT;
				Current_Direction = Direction::RIGHT;
				*dx = 1;
			}

			if (CheckHitKey(KEY_INPUT_UP) == 1) {
				Current_Anim_ID = AnimID::Anim_ID_UP;
				Current_Direction = Direction::UP;
				*dy = -1;
			}

			if (CheckHitKey(KEY_INPUT_DOWN) == 1) {
				Current_Anim_ID = AnimID::Anim_ID_DOWN;
				Current_Direction = Direction::DOWN;
				*dy = 1;
			}

			
		}
	
		/*停止時、その方向を向いたまま止まる*/
		if (Current_Anim_ID == AnimID::Anim_ID_STOP && Current_Direction == Direction::LEFT) {
		Current_Anim_ID = AnimID::Anim_ID_STOP_LEFT;
		}
	
		else if (Current_Anim_ID == AnimID::Anim_ID_STOP && Current_Direction == Direction::RIGHT) {
		Current_Anim_ID = AnimID::Anim_ID_STOP_RIGHT;
		}
	
		else if (Current_Anim_ID == AnimID::Anim_ID_STOP && Current_Direction == Direction::UP) {
		Current_Anim_ID = AnimID::Anim_ID_STOP_UP;
		}
	
		else if (Current_Anim_ID == AnimID::Anim_ID_STOP && Current_Direction == Direction::DOWN) {
		Current_Anim_ID = AnimID::Anim_ID_STOP_DOWN;
		}

	}
}

void DynamicObject::getCell(int* x, int* y) const {
	*x = ( mX - 16000 ) / 32000;
	*y = ( mY - 16000 ) / 32000;
}

bool DynamicObject::isIntersectWall(int x, int y, int wallX, int wallY) {
	int wx = convertCellToInner(wallX);
	int wy = convertCellToInner(wallY);

	int al = x - HALF_SIZE; //left A
	int ar = x + HALF_SIZE; //right A
	int bl = wx - 16000; //left B
	int br = wx + 16000; //right B
	if ((al < br) && (ar > bl)) {
		int at = y - HALF_SIZE; //top A
		int ab = y + HALF_SIZE; //bottom A
		int bt = wy - 16000; //top B
		int bb = wy + 16000; //bottom B
		if ((at < bb) && (ab > bt)) {
			return true;
		}
	}
	return false;
}

bool DynamicObject::isIntersectWall(int wallX, int wallY) {
	return isIntersectWall(mX, mY, wallX, wallY);
}

bool DynamicObject::hasBombButtonPressed() const {

	if (mType == TYPE_PLAYER) {
		if (mPlayerID == 0) {
			return (CheckHitKey(KEY_INPUT_Q) == 1);
		}
		else if (mPlayerID == 1) {
			return (CheckHitKey(KEY_INPUT_M) == 1);
		}
	}
	return false;
}

void DynamicObject::WeaponSet(int* x, int* y){
	if (mWD == WEAPON_RIGHT) {
		*x += 1;
	}else if (mWD == WEAPON_LEFT) {
		*x -= 1;
	}else if (mWD == WEAPON_UP) {
		*y -= 1;
	}else if (mWD == WEAPON_DOWN) {
		*y += 1;
	}
}

void DynamicObject::WeaponRoll(int direction) {

		int curCount = (weaponCount / 15 % 2);
		
		if (direction == 0) {

			if (curCount == 1){
				if (weaponflag == false) {

					if (mWD == WEAPON_RIGHT) {
						mWD = WEAPON_DOWN;
					}
					else if (mWD == WEAPON_LEFT) {
						mWD = WEAPON_UP;
					}
					else if (mWD == WEAPON_UP) {
						mWD = WEAPON_RIGHT;
					}
					else if (mWD == WEAPON_DOWN) {
						mWD = WEAPON_LEFT;
					}
					printf("switch ON");
					weaponflag = true;
				}
			}
			else {
				weaponflag = false;
			}

			printf("DEBUG::RIGHT::%d\n", weaponCount);
			
		}
		else if (direction == 1) {

			if (curCount == 1) {
				if (weaponflag == false) {

					if (mWD == WEAPON_RIGHT) {
						mWD = WEAPON_UP;
					}
					else if (mWD == WEAPON_LEFT) {
						mWD = WEAPON_DOWN;
					}
					else if (mWD == WEAPON_UP) {
						mWD = WEAPON_LEFT;
					}
					else if (mWD == WEAPON_DOWN) {
						mWD = WEAPON_RIGHT;
					}
					printf("switch ON");
					weaponflag = true;
				}
			}
			else {
				weaponflag = false;
			}

			printf("DEBUG::LEFT::%d\n", weaponCount);
		}
		else {
			printf("DEBUG::WeaponRollError");
		}
	
}

void DynamicObject::die() {
	mType = TYPE_NONE;
}

bool DynamicObject::isDead() const {
	return (mType == TYPE_NONE);
}