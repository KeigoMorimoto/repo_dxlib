#ifndef INCLUDED_LOAD_H
#define INCLUDED_LOAD_H

#include "iostream"
#include "Base.h"
#include "SceneMng.h"
#include "DxLib.h"
#include <map>

class SceneMng;

class Load : public Base {
public:
	Load();
	~Load();
	void update(SceneMng*);


private:
	void LoadingSounds();
	void LoadingImage();

	int ghandle[16];
	//int shandle[16];
	int stagehandle[112];
	int yajirusihandle[300];

	int LoadHandle[5];
	int current;
	//map<string, int> sounds_map;
	bool LoadFrag;
};


#endif