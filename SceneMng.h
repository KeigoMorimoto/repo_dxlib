#ifndef INCLUDED_SCENEMNG_H
#define INCLUDED_SCENEMNG_H

#include"DxLib.h"
//#include"Base.h"
#include"macro.h"

#include"Title.h"
#include"Game.h"
#include"Menu.h"
#include"Load.h"
#include"State.h"
#include"Clear.h"

#include "AnimDraw.h"
#include "SoundBox.h"

//class AnimDraw;//前方宣言
//class SoundBox;
class Base; //前方宣言


	class SceneMng {


	public:
		enum NextSequence {
			NEXT_TITLE,
			NEXT_GAME,
			NEXT_MENU,
			NEXT_LOAD,
			NEXT_GAME_OVER,
			NEXT_ENDING,
			NEXT_CLEAR,

			NEXT_NONE,
		};

		
		enum Mode {
			MODE_1P,
			MODE_2P,
			MODE_NONE,
		};
		
		void update();
		void moveTo(NextSequence);
		
		Mode mode() const;
		void setMode(Mode);

		static void create();
		static void destroy();
		static SceneMng* instance();

		
		static int getAnimCnt();
		
		/*
		State* state();
		void drawState();
		void startLoading();
		*/
	private:
		SceneMng();
		~SceneMng();

		static int animcounter;

		NextSequence mNextSequence;
		Mode mMode;

		Base* mBase;

		//State* mState;

		static SceneMng* mInstance;

		

	};


#endif
