#include<iostream>

#include"SceneMng.h"
#include"DxLib.h"
#include "FPS.h"
#include "AnimDraw.h"

int main() {

	SetMainWindowText("Arrow Battle");
	ChangeWindowMode(TRUE); // ウィンドウモードに設定
	SetGraphMode(800, 640, 32);//画面サイズ指定
	DxLib_Init();   // DXライブラリ初期化処理

	Fps fps;

	SetDrawScreen(DX_SCREEN_BACK);           // 描画先を裏画面に設定


	while(ScreenFlip() == 0 && ProcessMessage() == 0 && ClearDrawScreen() == 0 && CheckHitKey(KEY_INPUT_ESCAPE) == 0) {

		// fps処理
		fps.Updata();
		fps.Draw();
		fps.Wait();

		if (!SceneMng::instance()) {
			SceneMng::create();
		}

		SceneMng::instance()->update();
	}

	SceneMng::instance()->destroy();
	DxLib_End();    // DXライブラリ終了処理
}

