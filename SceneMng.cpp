#include"SceneMng.h"
	
	SceneMng* SceneMng::mInstance = 0;

	void SceneMng::create() {
		mInstance = new SceneMng();
	}

	void SceneMng::destroy() {
		SAFE_DELETE(mInstance);
	}

	SceneMng* SceneMng::instance() {
		return mInstance;
	}

	SceneMng::SceneMng() :
		mNextSequence(NEXT_NONE),
		mBase(0) {

		AnimDraw::create();
		SoundBox::create();
		//最初に作るのはタイトル
		mBase = new Title();
	}

	SceneMng::~SceneMng() {
		AnimDraw::destroy();
		SoundBox::destroy();
		//残っていれば抹殺
		//SAFE_DELETE(mInstance);
	}

	void SceneMng::update() {
		mBase->update(this);
		
		//遷移判定
		switch (mNextSequence) {

		case NEXT_TITLE:
			SAFE_DELETE(mBase);
			mBase = new Title();
			mNextSequence = NEXT_NONE; //戻す
			break;
		case NEXT_MENU:
			SAFE_DELETE(mBase);
			mBase = new Menu();
			mNextSequence = NEXT_NONE; //戻す
			break;
		case NEXT_GAME:
			SAFE_DELETE(mBase);
			mBase = new Game();
			mNextSequence = NEXT_NONE;
			break;
		case NEXT_LOAD:
			SAFE_DELETE(mBase);
			mBase = new Load();
			mNextSequence = NEXT_NONE;
			break;
		case NEXT_CLEAR:
			SAFE_DELETE(mBase);
			mBase = new Clear();
			mNextSequence = NEXT_NONE;
			break;
		}
		animcounter = animcounter + 1;
	}

		
	

	void SceneMng::moveTo(NextSequence next) {
		//ASSERT(mNextSequence == NEXT_NONE); //これ以外ありえない
		mNextSequence = next;
	}

	void SceneMng::setMode(Mode mode) {
		mMode = mode;
	}

	SceneMng::Mode SceneMng::mode() const {
		return mMode;
	}

	int SceneMng::getAnimCnt() {
		return animcounter;
	}

	int SceneMng::animcounter = 0;
	/*
	State* SceneMng::state() {
		return mState;
	}

	void SceneMng::drawState() {
		mState->draw();
	}

	void SceneMng::startLoading() {
		mState = new State(0);
	}
	*/