#include "Menu.h"

int Menu::stageID = 0;

Menu::Menu() {
	MenuCount = 0;

	LoadFrag = false;
	LoadingImage();

}

Menu::~Menu() {

}

void Menu::update(SceneMng* SceneMng) {

	DrawGraph(0, 0, MenuHandle[0], TRUE);

	DrawGraph(50, 250, MenuHandle[3], TRUE);
	DrawGraph(700, 250, MenuHandle[4], TRUE);
	DrawGraph(50, 560, MenuHandle[5], TRUE);


	if (CheckHitKey(KEY_INPUT_RIGHT) == 1) {
		MenuCount++;
		if (MenuCount == 30) {
			stageID = 1;
		}
	}
	else if (CheckHitKey(KEY_INPUT_LEFT) == 1) {
		MenuCount++;
		if (MenuCount == 30) {
			stageID = 0;
		}
	}
	else {
		MenuCount = 0;
	}
	if (stageID == 0) {
		DrawGraph(220, 150, MenuHandle[1], TRUE);
	}
	else if (stageID == 1) {
		DrawGraph(220, 150, MenuHandle[2], TRUE);
	}





	if (CheckHitKey(KEY_INPUT_RETURN) == 1) {
		SoundBox::instance()->SoundsMap_play("puyon");
		SceneMng->moveTo(SceneMng::NEXT_LOAD);
	}
}

void Menu::LoadingImage() {
	//MENU
	MenuHandle[0] = LoadGraph("Image/STAGESLECT/STAGESLECT.png");

	MenuHandle[1] = LoadGraph("Image/All_Stage/Stage1.png");
	MenuHandle[2] = LoadGraph("Image/All_Stage/stage2.png");
	MenuHandle[3] = LoadGraph("Image/Menu/LEFT.png");
	MenuHandle[4] = LoadGraph("Image/Menu/RIGHT.png");
	MenuHandle[5] = LoadGraph("Image/Menu/MENU.png");
	LoadFrag = true;

}


int Menu::getStageID() {
	return  stageID;
}


