#ifndef INCLUDED_BULLETMANAGER_H
#define INCLUDED_BULLETMANAGER_H

#include "AnimDraw.h"
#include "macro.h"
#include "Bullet.h"

#define BULLET_NUM 1

class Bullet;

class BulletManager {
public:

	BulletManager();
	~BulletManager();

	void shot(int mx, int my, int direction, int player);
	void draw() const;
	void updata();
	bool GotShot(int mx,int my);
	
	void GetBulletPosition();
	bool isIntersectWall(int wallCellX, int wallCellY);
	void OffBullet(int player, int i);

	static void create();
	static void destroy();
	static BulletManager* instance();


	int oneBulletPositionX[BULLET_NUM];
	int oneBulletPositionY[BULLET_NUM];

	int twoBulletPositionX[BULLET_NUM];
	int twoBulletPositionY[BULLET_NUM];

private:
	static BulletManager* mBulletManager;

	//int oneBulletNum;
	//int twoBulletNum;

	Bullet* oneBullet[BULLET_NUM];
	Bullet* twoBullet[BULLET_NUM];
};

#endif