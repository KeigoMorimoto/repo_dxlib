#include "BulletManager.h"

BulletManager* BulletManager::mBulletManager = 0;

//内部単位
static int convertCellToInner(int x) {
	return x * 32000 + 16000;
}
//内部単位から画素単位
static int convertInnerToPixel(int x) {
	return  (x + 500 - 16000) / 1000;
}

BulletManager::BulletManager() {
	
	for (int num = 0; num < BULLET_NUM; num++)
	{
		oneBullet[num] = NULL;
		twoBullet[num] = NULL;
	}
}

BulletManager::~BulletManager() {

	for (int num = 0; num < BULLET_NUM; num++)
	{
		//SAFE_DELETE(oneBullet[num]);
		//SAFE_DELETE(twoBullet[num]);

		delete oneBullet[num];
		delete twoBullet[num];
	}
}

void BulletManager::create() {
	mBulletManager = new BulletManager();
}

void BulletManager::destroy() {
	SAFE_DELETE(mBulletManager);
}

BulletManager* BulletManager::instance() {
	return mBulletManager;
}


void BulletManager::shot(int mx, int my, int direction, int player){
	
	
	switch (player){
	case 0:
		
		for (int num = 0; num < BULLET_NUM; num++)
		{
			if (oneBullet[num] == NULL)
			{
				oneBullet[num] = new Bullet(mx, my, direction, player);
				break;
			}
		}

		break;

	case 1:

		for (int num = 0; num < BULLET_NUM; num++)
		{
			if (twoBullet[num] == NULL)
			{
				twoBullet[num] = new Bullet(mx, my, direction, player);
				break;
			}
		}

		break;
	default:
		break;
	}
}

void BulletManager::draw() const{
	for (int num = 0; num < BULLET_NUM; num++)
	{
		if (oneBullet[num] != NULL)
		{
			oneBullet[num]->draw();
		}
	}

	for (int num = 0; num < BULLET_NUM; num++)
	{
		if (twoBullet[num] != NULL)
		{
			twoBullet[num]->draw();
		}
	}
}

void BulletManager::updata(){


	for (int num = 0; num < BULLET_NUM; num++)
	{
		if (oneBullet[num] != NULL)
		{
			if (oneBullet[num]->updata() == true) {
				oneBullet[num] = NULL;
			}
		}
	}

	for (int num = 0; num < BULLET_NUM; num++)
	{
		if (twoBullet[num] != NULL)
		{
			if (twoBullet[num]->updata() == true) {
				twoBullet[num] = NULL;
			}
		}
	}
}

bool BulletManager::GotShot(int mx,int my) {
	
	for (int num = 0; num < BULLET_NUM; num++)
	{
		if (oneBullet[num] != NULL)
		{
			if (oneBullet[num]->GotShot(mx, my) == true) {
				return true;
			}
		}
	}

	for (int num = 0; num < BULLET_NUM; num++)
	{
		if (twoBullet[num] != NULL)
		{
			if (twoBullet[num]->GotShot(mx, my) == true) {
				return true;
			}
		}
	}

	return false;
}

void BulletManager::GetBulletPosition() {

	int x, y;

	for (int num = 0; num < BULLET_NUM; num++)
	{
		if (oneBullet[num] != NULL)
		{
			oneBullet[num]->getCell(&x, &y);
			oneBulletPositionX[num] = x;
			oneBulletPositionY[num] = y;
		}
		else {
			oneBulletPositionX[num] = -1;
			oneBulletPositionY[num] = -1;
		}
	}

	for (int num = 0; num < BULLET_NUM; num++)
	{
		if (twoBullet[num] != NULL)
		{
			twoBullet[num]->getCell(&x, &y);
			twoBulletPositionX[num] = x;
			twoBulletPositionY[num] = y;
		}
		else {
			twoBulletPositionX[num] = -1;
			twoBulletPositionY[num] = -1;
		}
	}
}


bool BulletManager::isIntersectWall(int wallCellX, int wallCellY) {

	int wx = convertCellToInner(wallCellX);
	int wy = convertCellToInner(wallCellY);


	for (int num = 0; num < BULLET_NUM; num++)
	{
		if (oneBullet[num] != NULL)
		{
			if (oneBullet[num]->isIntersectWall(wx, wy) == true) {
				return true;
			}
		}
	}

	for (int num = 0; num < BULLET_NUM; num++)
	{
		if (twoBullet[num] != NULL)
		{
			if (twoBullet[num]->isIntersectWall(wx, wy) == true) {
				return true;
			}
		}
	}

	return false;
}

void BulletManager::OffBullet(int player, int num) {
	if (player == 0) {
		oneBullet[num] = NULL;
	}
	else if(player == 1){
		twoBullet[num] = NULL;
	}
}
