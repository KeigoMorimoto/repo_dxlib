#ifndef INCLUDED_SOUNDBOX_H
#define INCLUDED_SOUNDBOX_H

#include <vector>
#include "Dxlib.h"
#include "SceneMng.h"

using namespace std;

class SoundBox {

public:
	vector<int> sounds;
	int bgm;
	string current_bgm;

	int set(int shandle);
	void play(int snum);
	void stop(int snum);
	void playbgm(int snum);


	void SoundsMap_set(string name,int shandle);
	void SoundsMap_play(string name);
	void SoundsMap_stop(string name);
	void SoundsMap_playbgm(string name);
	

	static void create();
	static void destroy();
	static SoundBox* instance();



private:
	static SoundBox* mSoundBoxInstance;
};

#endif // !INCLUDE_SOUNDBOX_H
