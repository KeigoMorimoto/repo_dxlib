#include"AnimDraw.h"

AnimDraw* AnimDraw::mAnimInstance = 0;

AnimDraw::AnimDraw() {
	aplist.push_back(animpat());
}


void AnimDraw::create() {
	mAnimInstance = new AnimDraw();
}

void AnimDraw::destroy() {
	SAFE_DELETE(mAnimInstance);
}

AnimDraw* AnimDraw::instance() {
	return mAnimInstance;
}

int AnimDraw::setImage(int apid, int ghandle) {
	
	if (apid >= aplist.size()) {
		aplist.push_back(animpat());
	
		apid = aplist.size() - 1;
	}

	aplist.at(apid).ghlist.push_back(ghandle);

	return(apid);
}

int AnimDraw::setGap(int apid, int gap) {
	
	if (apid >= aplist.size()) {
		aplist.push_back(animpat());

		apid = aplist.size() - 1;
	}

	aplist.at(apid).anigap = gap;

	return(apid);
}

void AnimDraw::draw(int apid, int x, int y) {
	
	if (aplist.at(apid).ghlist.size() > 1) {
		int curpat = SceneMng::getAnimCnt() / aplist.at(apid).anigap % aplist.at(apid).ghlist.size();
		DrawGraph(x, y, aplist.at(apid).ghlist.at(curpat), true);
	}
	else {
		DrawGraph(x, y, aplist.at(apid).ghlist.at(0), true);
	}
}

void AnimDraw::draw(int apid, int x, int y, float rad) {
	
	if (aplist.at(apid).ghlist.size() > 1) {
		int curpat = SceneMng::getAnimCnt() / aplist.at(apid).anigap % aplist.at(apid).ghlist.size();
		DrawRotaGraph(x, y, 1.0, rad ,aplist.at(apid).ghlist.at(curpat), true, false);
	}
	else {
		DrawRotaGraph(x, y, 1.0, rad, aplist.at(apid).ghlist.at(0), true, false);
	}
}




