#include "Title.h"


	Title::Title(){
		SoundBox::instance()->SoundsMap_set("puyon", LoadSoundMem("Sounds/puyon1.wav"));

		LoadFrag = false;
		LoadingImage();

	}

	Title::~Title() {
		
	}

	void Title::update(SceneMng* SceneMng) {
		//printf("TITLE\n");
		//DrawFormatString(1,1,Green, "座標[]"); // 文字を描画する
		//int Green = GetColor(0, 255, 0);

		DrawFormatString(400, 10, GetColor(0, 255, 0), "TITLE"); // 文字を描画する
		
		DrawGraph(0, 0, TitleHandle[4], TRUE);

		
		current = SceneMng::getAnimCnt() / 30 % 4;

		switch (current)
		{
		case 0:
			DrawGraph(130, 0, TitleHandle[0], TRUE);
			break;
		case 1:
			DrawGraph(130, 0, TitleHandle[1], TRUE);
			break;
		case 2:
			DrawGraph(130, 0, TitleHandle[2], TRUE);
			break;
		case 3:
			DrawGraph(130, 0, TitleHandle[3], TRUE);
			break;
		}

		DrawGraph(50, 550, TitleHandle[5], TRUE);



		if (CheckHitKey(KEY_INPUT_SPACE) == 1 && LoadFrag == true) {
			SoundBox::instance()->SoundsMap_play("puyon");
			SceneMng->moveTo(SceneMng::NEXT_MENU);
		}
	}

	void Title::LoadingImage() {
		//TITLE
		TitleHandle[0] = LoadGraph("Image/TITLE/Arrow1.png");
		TitleHandle[1] = LoadGraph("Image/TITLE/Arrow2.png");
		TitleHandle[2] = LoadGraph("Image/TITLE/Arrow3.png");
		TitleHandle[3] = LoadGraph("Image/TITLE/Arrow4.png");

		TitleHandle[4] = LoadGraph("Image/TITLE/BackGround.png");
		TitleHandle[5] = LoadGraph("Image/TITLE/TITLE.png");
		
		LoadFrag = true;
	}


