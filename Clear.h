#ifndef INCLUDED_CLEAR_H
#define INCLUDED_CLEAR_H

#include "iostream"
#include "Base.h"
#include "SceneMng.h"
#include "DxLib.h"
//#include "State.h"
#include "macro.h"

class SceneMng;
class State;

class Clear : public Base {
public:
	Clear ();
	~Clear();
	void update(SceneMng*);


private:
	//Image * mImage; //タイトル画面画像
	//int mCursorPosistion;
	//State * mState;
	void LoadingImage();
	int ClearHandle[3];
	bool LoadFrag;
};


#endif