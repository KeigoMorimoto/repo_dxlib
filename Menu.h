#ifndef INCLUDED_MENU_H
#define INCLUDED_MENU_H

#include "iostream"
#include "Base.h"
#include "SceneMng.h"
#include "DxLib.h"

class SceneMng;

class Menu : public Base {
public:
	Menu();
	~Menu();
	void update(SceneMng*);

	static int getStageID();
	static int stageID;

private:
	//Image * mImage; //タイトル画面画像
	//int mCursorPosistion;

	bool LoadFrag;
	void LoadingImage();

	int MenuHandle[6];
	int MenuCount;
	
};


#endif