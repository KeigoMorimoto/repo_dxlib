#include "Clear.h"

Clear::Clear() {
	//mState = new State();
	//AnimDraw::create();
	//LoadDivGraph("char.png", 16, 4, 4, 32, 32, ghandle);

	LoadFrag = false;
	LoadingImage();
	
}

Clear::~Clear() {
	//SAFE_DELETE(mState);
	//AnimDraw::destroy();
}

void Clear::update(SceneMng* SceneMng) {

	//DrawFormatString(10, 10, GetColor(0, 255, 0), "Clear"); // 文字を描画する

	printf("%d\n",State::CheckClearFlag());
	if (State::CheckClearFlag() == 0) {
		DrawGraph(0, 0, ClearHandle[0], TRUE);
	}
	else if (State::CheckClearFlag() == 1) {
		DrawGraph(0, 0, ClearHandle[1], TRUE);
	}
	else {
		DrawGraph(0, 0, ClearHandle[2], TRUE);
	}

	if (CheckHitKey(KEY_INPUT_Z) == 1) {
		SceneMng->moveTo(SceneMng::NEXT_TITLE);
	}
}

void Clear::LoadingImage() {
	//Clear
	ClearHandle[0] = LoadGraph("Image/Clear/1P.png");
	ClearHandle[1] = LoadGraph("Image/Clear/2P.png");
	ClearHandle[2] = LoadGraph("Image/Clear/DRAW.png");

	LoadFrag = true;
}
