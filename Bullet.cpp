#include"Bullet.h"

static const int BULLET_SPEED = 4000;
static const int HALF_SIZE = 8000;

//内部単位
static int convertCellToInner(int x) {
	return x * 32000 + 16000;
}
//内部単位から画素単位
static int convertInnerToPixel(int x) {
	return  (x + 500 - 16000) / 1000;
}

Bullet::Bullet(int mx, int my, int direction, int player) :
	mX(mx),
	mY(my){

	switch (direction)
	{
	case 0:
		mDirection = LEFT;
		break;
	case 1:
		mDirection = RIGHT;
		break;
	case 2:
		mDirection = UP;
		break;
	case 3:
		mDirection = DOWN;
		break;
	default:
		mDirection = NULL_Direction;
		break;
	}

	switch (player)
	{
	case 0:
		mPlayer = oneP;
		break;
	case 1:
		mPlayer = twoP;
		break;
	default:
		mDirection = NULL_Direction;
		break;
	}

}

Bullet::~Bullet() {

}

/*
void Bullet::set(int mx, int my, Direction direction, Player player) {

	switch (player)
	{
	case Bullet::oneP:
		mPlayer = oneP;
		break;
	case Bullet::twoP:
		mPlayer = twoP;
		break;
	default:
		break;
	}

	mX = mx;
	mY = my;

	mDirection = direction;

}
*/

bool Bullet::updata() {
	int dx, dy;

	getVelocity(&dx, &dy);

	int movedX = mX + dx;
	int movedY = mY + dy;

	mX = movedX;
	mY = movedY;

	if (mX < 0 || mX > 800000 || mY < 0 || mY > 576000) {
		return true;
	}

	return false;
}

void Bullet::draw()const {

	int dstX = convertInnerToPixel(mX);
	int dstY = convertInnerToPixel(mY);


	if (mDirection == RIGHT) {
		AnimDraw::instance()->draw(21, dstX, dstY);
	}
	else if (mDirection == LEFT) {
		AnimDraw::instance()->draw(22, dstX, dstY);
	}
	else if (mDirection == UP) {
		AnimDraw::instance()->draw(23, dstX, dstY);
	}
	else if (mDirection == DOWN) {
		AnimDraw::instance()->draw(24, dstX, dstY);
	}
	else {
		printf("DEBUG::BulletClassありえない方向");
	}
}


void Bullet::getVelocity(int* dx, int* dy) {
	//スピードを変数に格納
	int speedX, speedY;

	speedX = BULLET_SPEED;
	speedY = BULLET_SPEED;

	//向き取得
	getDirection(dx, dy);
	//速度計算
	*dx = *dx * speedX;
	*dy = *dy * speedY;


}


void Bullet::getDirection(int* dx, int* dy) {

	*dx = *dy = 0;

	if (mDirection == LEFT) {
		*dx = -1;
	}

	if (mDirection == RIGHT) {
		*dx = 1;
	}

	if (mDirection == UP) {
		*dy = -1;
	}

	if (mDirection == DOWN) {
		*dy = 1;
	}

}

bool Bullet::isIntersectWall(int x, int y, int wallX, int wallY) {
	
	//int wx = convertCellToInner(wallX);
	//int wy = convertCellToInner(wallY);

	int wx = wallX;
	int wy = wallY;

	int al = x - HALF_SIZE; //left A
	int ar = x + HALF_SIZE; //right A
	int bl = wx - 16000; //left B
	int br = wx + 16000; //right B
	if ((al < br) && (ar > bl)) {
		int at = y - HALF_SIZE; //top A
		int ab = y + HALF_SIZE; //bottom A
		int bt = wy - 16000; //top B
		int bb = wy + 16000; //bottom B
		if ((at < bb) && (ab > bt)) {
			return true;
		}
	}
	return false;
}

bool Bullet::isIntersectWall(int wallX, int wallY) {
	return isIntersectWall(mX, mY, wallX, wallY);
}

bool Bullet::GotShot(int mx, int my) {
	return(isIntersectWall(mx, my));
}

void Bullet::getCell(int* x, int* y) const {
	*x = (mX - 16000) / 32000;
	*y = (mY - 16000) / 32000;
}

