#ifndef INCLUDED_DYNAMIC_OBJECT_H
#define INCLUDED_DYNAMIC_OBJECT_H

#include "AnimDraw.h"

class DynamicObject {
public:

	enum Type {
		TYPE_PLAYER,
		//TYPE_ENEMY,
		//TYPE_WEAPON,
		
		TYPE_NONE, //死亡
	};
	/*
	//マス描画関数
	enum ImageID {
		IMAGE_ID_UNKNOW = 0, //UNKNOW

		IMAGE_ID_1P = 1, //1P
		IMAGE_ID_2P = 2, //2P
		IMAGE_ID_ENEMY = 3, //ENEMY
	};
	*/
	enum AnimID {
		Anim_ID_STOP,
		Anim_ID_STOP_RIGHT,
		Anim_ID_STOP_LEFT,
		Anim_ID_STOP_UP,
		Anim_ID_STOP_DOWN,
		Anim_ID_LEFT,
		Anim_ID_RIGHT,
		Anim_ID_UP,
		Anim_ID_DOWN,
	};

	enum Direction {
		LEFT,
		RIGHT,
		UP,
		DOWN
	};

	enum WEAPON_Direction {
		WEAPON_LEFT,
		WEAPON_RIGHT,
		WEAPON_UP,
		WEAPON_DOWN,

		NULL_Direction
	};


	DynamicObject();
	void set(int x, int y, Type);
	void draw() const;

	void WeaponSet(int* x, int* y);

	void move(int* wallsX, int* wallsY, int wallNumber);
	void getCell(int* x, int* y) const;
	
	bool hasBombButtonPressed() const;	//爆弾ボタンが押されたか調べる
	bool isIntersectWall(int wallCellX, int wallCellY);

	int mPlayerID;//プレイヤー番号
	//int mWeaponID;//武器番号

	Type mType;
	WEAPON_Direction mWD;
	int mX;
	int mY;


	//int curDirection;
	int weaponCount;
	bool weaponflag;
	void WeaponRoll(int direction);

	int ButtonCount;


	void die(); 
	bool isDead() const; 

	//壁用
	//bool isIntersectWall(int wallCellX, int wallCellY);
	//void doCollisionReactionToDynamic(DynamicObject* another);
	//bool isIntersect(const DynamicObject& o) const;

	//便利関数群
	

	/*
	bool isPlayer() const;
	bool isEnemy() const;
	void die(); //死にます(mTypeをNONEにすることで表現)
	bool isDead() const; //死んでますか？


	//プレイヤー専用
	int mBombPower; //爆発力
	int mBombNumber; //爆弾数

	*/


	

	//敵専用
	//int mDirectionX;
	//int mDirectionY;

private:
	
	//今フレームの移動量を取得
	void getVelocity(int* dx, int* dy) ;

	//移動方向を取得
	void getDirection(int* dx, int* dy) ;
	
	//壁用
	static bool isIntersectWall(int x, int y, int wallCellX, int wallCellY);
	

	//Image* mImage; //画像
	int Current_Anim_ID;
	int Current_Direction;
};

#endif