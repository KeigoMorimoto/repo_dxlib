#ifndef INCLUDED_ANIMDRAW_H
#define INCLUDED_ANIMDRAW_H

#include <vector>
#include "Dxlib.h"
#include "SceneMng.h"

using namespace std;


class AnimDraw {

public:


	struct animpat
	{
		vector<int> ghlist; //gHandle
		int anigap; //�\���Ԋu
		 animpat() { anigap = 1; }
	};

	vector<animpat> aplist;

	AnimDraw();

	int setImage(int apid, int ghandle);
	int setGap(int apid, int gap);

	void draw(int apid, int x, int y);
	void draw(int apid, int x, int y, float rad);

	static AnimDraw* instance();
	static void create();
	static void destroy();


private:
	static AnimDraw* mAnimInstance;

};



#endif // !INCLUDE_ANIM_H
