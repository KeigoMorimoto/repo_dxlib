#ifndef INCLUDED_TITLE_H
#define INCLUDED_TITLE_H

#include "iostream"
#include "Base.h"
#include "SceneMng.h"
#include "DxLib.h"

class SceneMng;

class Title : public Base {
	public:
		Title();
		~Title();
		void update(SceneMng*);
	private:
		//Image * mImage; //タイトル画面画像
		//int mCursorPosistion;

		int TitleHandle[6];
		void LoadingImage();
		bool LoadFrag;
		int current;
	};


#endif