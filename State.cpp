#include "State.h"

State::State():
	mWIDTH(-1),
	mHEIGHT(-1){

	mStageID = Menu::getStageID();
	

	//stageのロード
	loadStage();

	//Player人数
	mDynamicObjectNumber = 2;

	mDynamicObjects = new DynamicObject[2];
	//1P
	mDynamicObjects[0].set(8, 8, DynamicObject::TYPE_PLAYER);
	mDynamicObjects[0].mPlayerID = 0;
	mDynamicObjects[0].mWD = DynamicObject::WEAPON_RIGHT;
	//2P
	mDynamicObjects[1].set(16, 9, DynamicObject::TYPE_PLAYER);
	mDynamicObjects[1].mPlayerID = 1;
	mDynamicObjects[1].mWD = DynamicObject::WEAPON_LEFT;

	//BulletManagerの生成
	if (!BulletManager::instance()) {
		BulletManager::create();
	}

	TookTime = 0;
	CurrentTime = StartTime = SceneMng::getAnimCnt();

	AfterClearTime = 0;

	SoundBox::instance()->SoundsMap_play("bgm");
}

State::~State() {
	BulletManager::destroy();
	SAFE_DELETE(mfile);
	SAFE_DELETE(mDynamicObjects);

	SoundBox::instance()->SoundsMap_stop("bgm");
}

void State::update(SceneMng* SceneMng) {

	for (int i = 0; i < mDynamicObjectNumber; i++) {
		DynamicObject& o = mDynamicObjects[i];

		//現在のセル位置
		int x, y;
		o.getCell(&x, &y);

		int wallsX[9];
		int wallsY[9];
		int wallNumber = 0;

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				int tx = x + i - 1;
				int ty = y + j - 1;
				const StaticObject& so = mStaticObjects(tx, ty);

				//OBJECT01,02,WALLを壁判定する
				if (so.checkFlag(StaticObject::FLAG_OBJECT01 | StaticObject::FLAG_OBJECT02 | StaticObject::FLAG_WALL))
				{
						wallsX[wallNumber] = x + i - 1;
						wallsY[wallNumber] = y + j - 1;
						wallNumber++;
				}
			}
		}
		//周囲9マスの壁とオブジェクトの位置情報を渡して移動する
		o.move(wallsX, wallsY, wallNumber);
		
		
		//移動後の位置で周囲9マスと衝突判定していろいろな反応
		for (int i = 0; i < 3; ++i) {
			for (int j = 0; j < 3; ++j) {
				StaticObject& so = mStaticObjects(x + i - 1, y + j - 1);
				if (o.isIntersectWall(x + i - 1, y + j - 1)) { //触ってます
					if (so.checkFlag(StaticObject::FLAG_ROLL_R)) {
						//printf("right\n");
						o.weaponCount++;
						o.WeaponRoll(0);
					}else if (so.checkFlag(StaticObject::FLAG_ROLL_L)) {
						//printf("left\n");
						o.weaponCount++;
						o.WeaponRoll(1);
					}else {
						o.weaponCount = 0;
					}
				}
			}
		}
		
		
		//ShotButtonが押された
		if (o.hasBombButtonPressed()) {

			if ( o.ButtonCount  >= 800 ) {
				if (o.mWD == DynamicObject::WEAPON_LEFT) {
					BulletManager::instance()->shot(o.mX - 32000, o.mY, 0, o.mPlayerID);
					o.ButtonCount = 0;
				}
				else if (o.mWD == DynamicObject::WEAPON_RIGHT) {
					BulletManager::instance()->shot(o.mX + 32000, o.mY, 1, o.mPlayerID);
					o.ButtonCount = 0;
				}
				else if (o.mWD == DynamicObject::WEAPON_UP) {
					BulletManager::instance()->shot(o.mX, o.mY - 32000, 2, o.mPlayerID);
					o.ButtonCount = 0;
				}
				else if (o.mWD == DynamicObject::WEAPON_DOWN) {
					BulletManager::instance()->shot(o.mX, o.mY + 32000, 3, o.mPlayerID);
					o.ButtonCount = 0;
				}
			}
			else {
				o.ButtonCount+= 170;
			}
		}
		else {
			o.ButtonCount = 0;
		}
	}

	//BulletManagerを使用して弾をupdata
	BulletManager::instance()->updata();

	//現在のBulletの位置を取得
	BulletManager::instance()->GetBulletPosition();

	int x, y;
	
	//BulletとObjectの当たり判定
	for (int num = 0; num < BULLET_NUM; num++){
		
		//1P側の弾の衝突を調べる
		x = BulletManager::instance()->oneBulletPositionX[num];
		y = BulletManager::instance()->oneBulletPositionY[num];

		if (x != -1 && y != -1) {
			
			//移動後の位置で周囲9マスと衝突判定していろいろな反応
			for (int i = 0; i < 3; ++i) {
				for (int j = 0; j < 3; ++j) {
					StaticObject& so = mStaticObjects(x + i - 1, y + j - 1);
					if (BulletManager::instance()->isIntersectWall(x + i - 1, y + j - 1)) { //触ってます
						if (so.checkFlag(StaticObject::FLAG_WALL | StaticObject::FLAG_OBJECT01 | StaticObject:: FLAG_OBJECT02)) {

							BulletManager::instance()->OffBullet(0, num);
						}
					}
				}
			}
			
		}

		//2P側の弾の衝突を調べる
		x = BulletManager::instance()->twoBulletPositionX[num];
		y = BulletManager::instance()->twoBulletPositionY[num];

		if (x != -1 && y != -1) {

			//移動後の位置で周囲9マスと衝突判定していろいろな反応
			for (int i = 0; i < 3; ++i) {
				for (int j = 0; j < 3; ++j) {
					StaticObject& so = mStaticObjects(x + i - 1, y + j - 1);
					if (BulletManager::instance()->isIntersectWall(x + i - 1, y + j - 1)) { //触ってます
						if (so.checkFlag(StaticObject::FLAG_WALL | StaticObject::FLAG_OBJECT01 | StaticObject::FLAG_OBJECT02)) {

							BulletManager::instance()->OffBullet(1, num);
						}
					}
				}
			}

		}


	}

	//printf("\n");
	

	//弾がキャラにあたり判定
	if (BulletManager::instance()->GotShot(mDynamicObjects[1].mX, mDynamicObjects[1].mY)) {
		mDynamicObjects[1].die();
	}
	else if (BulletManager::instance()->GotShot(mDynamicObjects[0].mX, mDynamicObjects[0].mY)) {
		mDynamicObjects[0].die();
	}

	//クリア判定
	if ((mDynamicObjects[0].isDead() == true) && (mDynamicObjects[1].isDead() == true)) {
		//cout << "draw" << "\n";
		AfterClearTime++;
		ClearFlag = 2;
		//SceneMng->moveTo(SceneMng::NEXT_CLEAR);
	}
	else if (mDynamicObjects[0].isDead() == true) {
		//cout << "2P Win" << "\n";
		AfterClearTime++;
		ClearFlag = 1;
		//SceneMng->moveTo(SceneMng::NEXT_CLEAR);
	}
	else if (mDynamicObjects[1].isDead() == true) {
		//cout << "1P Win" << "\n";
		AfterClearTime++;
		ClearFlag = 0;
		//SceneMng->moveTo(SceneMng::NEXT_CLEAR);
	}

	if (AfterClearTime > AFTERCLEARTIME) {
		SceneMng->moveTo(SceneMng::NEXT_CLEAR);
	}
		
	CurrentTime = SceneMng::getAnimCnt();

	TookTime = ( CurrentTime - StartTime ) / 60;
	printf("%d\n", TookTime);
}

void State::draw() {

	//マップ描画
	for (int y = 0; y < mHEIGHT; y++) {
		for (int x = 0; x < mWIDTH; x++) {
			mStaticObjects(x, y).draw(x, y);
		}
	}
	
	//プレイヤー描画
	for (int i = 0; i < mDynamicObjectNumber; i++) {
		mDynamicObjects[i].draw();
	}

	//Bullet描画
	BulletManager::instance()->draw();
}

void State::setSize(const char* stageData, int size) {
	//const char* d = stageData; //読み込みポインタ
	mWIDTH = mHEIGHT = 0; //初期化
						  //現在位置
	int x = 0;
	int y = 0;

	for (int i = 0; i < size; i++) {
		switch (stageData[i]) {
		case '#': case ' ': case '1': case '2': case 'R': case 'L':
			x++;
			break;
		case '\n':
			y++;
			//最大値更新
			mWIDTH = (mWIDTH > x) ? mWIDTH : x;
			mHEIGHT = (mHEIGHT > y) ? mHEIGHT : y;
			x = 0;
			break;
		}
	}
}

void State::loadStage() {

	//StageDateの読み込み
	if (mStageID == 0) {	
		mfile = new File("Stage/stage1.txt");
	}
	//StageDateの読み込み
	if (mStageID == 1) {
		mfile = new File("Stage/stage2.txt");
	}

	//StageData ERROR
	if (!(mfile->data())) { 
		std::cout << "stage file could not be read." << std::endl;
	}

	const char* stageData = mfile->data();
	const int stageSize = mfile->size();

	//StageSizeの取得
	setSize(stageData,stageSize);
	printf("Debug::WIDTH:%d,HEIGHT:%d\n", mWIDTH, mHEIGHT);

	mStaticObjects.setSize(mWIDTH, mHEIGHT);

	int x = 0;
	int y = 0;

	for (int i = 0; i < stageSize; i++) {

		switch (stageData[i]) {
		case '#': 
			//printf("[%d]=#",i);
			mStaticObjects(x, y).setFlag(StaticObject::FLAG_WALL);
			x++;
			break;
		case ' ':
			//printf("[%d]= ", i);
			mStaticObjects(x, y).setFlag(StaticObject::FLAG_SPACE);
			x++;
			break;
		case '1':
			//printf("[%d]= ", i);
			mStaticObjects(x, y).setFlag(StaticObject::FLAG_OBJECT01);
			x++;
			break;
		case '2':
			//printf("[%d]= ", i);
			mStaticObjects(x, y).setFlag(StaticObject::FLAG_OBJECT02);
			x++;
			break;
		case 'R':
			//printf("[%d]= ", i);
			mStaticObjects(x, y).setFlag(StaticObject::FLAG_ROLL_R);
			x++;
			break;
		case 'L':
			//printf("[%d]= ", i);
			mStaticObjects(x, y).setFlag(StaticObject::FLAG_ROLL_L);
			x++;
			break;
		case '\n': x = 0; y++; break; //改行処理
		}

	}	
}

int State::ClearFlag = 0;

int State::CheckClearFlag() {
	return ClearFlag;
}