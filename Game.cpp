#include "Game.h"

Game::Game() {
	mState = new State();
	//AnimDraw::create();
	//LoadDivGraph("char.png", 16, 4, 4, 32, 32, ghandle);
}

Game::~Game() {
	SAFE_DELETE(mState);
	//AnimDraw::destroy();
}

void Game::update(SceneMng* SceneMng) {

	mState->update(SceneMng);
	mState->draw();

	//DrawFormatString(10, 10, GetColor(0, 255, 0), "Game"); // 文字を描画する

	if (CheckHitKey(KEY_INPUT_P) == 1) {
		SAFE_DELETE(mState);
		SceneMng->moveTo(SceneMng::NEXT_TITLE);
	}
}

