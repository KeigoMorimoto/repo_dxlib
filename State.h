#ifndef INCLUDED_STATE_H
#define INCLUDED_STATE_H

#include "AnimDraw.h"
#include "Array2D.h"
#include "SceneMng.h"
#include "DynamicObject.h"
#include "StaticObject.h"
#include "Bullet.h"
#include "BulletManager.h"
#include "Image.h"
#include "File.h"
//#include "Menu.h"

class File;
class DynamicObject;
class StaticObject;
class Bullet;
//class Menu;

#define AFTERCLEARTIME 180

class State {

public:

	

	State();
	~State();
	
	void update(SceneMng* SceneMng);
	void draw();

	//bool hasCleared() const;
	
	//どちらが勝ったか
	static int ClearFlag;
	static int CheckClearFlag();

private:

	void loadStage();


	int StartTime;
	int TookTime;
	int CurrentTime;

	int AfterClearTime;
	/*
	//クリア判定
	bool cleared = false;
	int clearedID = -1;
	
	//クリア経過時間
	int tooktime = 0;

	//敵のアイテムドロップ
	bool EnemyDownFrag = false;
	int EnemyItemX = -1;
	int EnemyItemY = -1;
	int item_rand = 0;

	int EnemyDownTime = 0;

	//炎設置。座標は爆弾の座標
	void setFire(int x, int y);
	*/

	

	//動かないオブジェクト
	//Array2D< StaticObject > mStaticObjects;
	//動くオブジェクト

	DynamicObject* mDynamicObjects;
	int mDynamicObjectNumber;

	
	int mStageID;
	int mWIDTH, mHEIGHT;	//StageSize
	
	File* mfile;
	void setSize(const char* stageData, int size);
	
	Array2D <StaticObject> mStaticObjects;
	//Image* mImage = new Image(); //画像
	//AnimDraw* mAnimDraw = new AnimDraw();
	//Bullet* mBullet;
	
};

#endif