#include "Load.h"


Load::Load() {
	LoadHandle[0] = LoadGraph("Image/Load/rload1.png");
	LoadHandle[1] = LoadGraph("Image/Load/rload2.png");
	LoadHandle[2] = LoadGraph("Image/Load/rload3.png");
	LoadHandle[3] = LoadGraph("Image/Load/rload4.png");
	LoadHandle[4] = LoadGraph("Image/Load/z.png");


	LoadFrag = false;

	LoadingSounds();
	LoadingImage();
}

Load::~Load() {
}

void Load::update(SceneMng* SceneMng) {

	current = SceneMng::getAnimCnt() / 30 % 4;

	switch (current)
	{
	case 0:
		DrawGraph(0, 0, LoadHandle[0], TRUE);
		break;
	case 1:
		DrawGraph(0, 0, LoadHandle[1], TRUE);
		break;
	case 2:
		DrawGraph(0, 0, LoadHandle[2], TRUE);
		break;
	case 3:
		DrawGraph(0, 0, LoadHandle[3], TRUE);
		break;
	}

	DrawGraph(100, 550, LoadHandle[4], TRUE);

	//DrawFormatString(10, 10, GetColor(0, 255, 0), "Load"); // ������`�悷��


	if (CheckHitKey(KEY_INPUT_Z) == 1 && LoadFrag == true) {
		SceneMng->moveTo(SceneMng::NEXT_GAME);
	}
}


void Load::LoadingSounds() {
	//SoundBox::instance()->set(LoadSoundMem("puyon1.wav"));
	SoundBox::instance()->SoundsMap_set("bgm",LoadSoundMem("Sounds/GAME/bgm_maoudamashii_ethnic22.mp3"));
}

void Load::LoadingImage() {

	//1P
	LoadDivGraph("Image/char.png", 16, 4, 4, 32, 32, ghandle);

	AnimDraw::instance()->setImage(0, ghandle[8]);
	AnimDraw::instance()->setGap(0, 1);

	AnimDraw::instance()->setImage(1, ghandle[4]);
	AnimDraw::instance()->setImage(1, ghandle[5]);
	AnimDraw::instance()->setImage(1, ghandle[6]);
	AnimDraw::instance()->setImage(1, ghandle[7]);
	AnimDraw::instance()->setGap(1, 6);

	AnimDraw::instance()->setImage(2, ghandle[12]);
	AnimDraw::instance()->setImage(2, ghandle[13]);
	AnimDraw::instance()->setImage(2, ghandle[14]);
	AnimDraw::instance()->setImage(2, ghandle[15]);
	AnimDraw::instance()->setGap(2, 6);

	AnimDraw::instance()->setImage(3, ghandle[0]);
	AnimDraw::instance()->setImage(3, ghandle[1]);
	AnimDraw::instance()->setImage(3, ghandle[2]);
	AnimDraw::instance()->setImage(3, ghandle[3]);
	AnimDraw::instance()->setGap(3, 6);

	AnimDraw::instance()->setImage(4, ghandle[8]);
	AnimDraw::instance()->setImage(4, ghandle[9]);
	AnimDraw::instance()->setImage(4, ghandle[10]);
	AnimDraw::instance()->setImage(4, ghandle[11]);
	AnimDraw::instance()->setGap(4, 6);

	AnimDraw::instance()->setImage(5, ghandle[4]);
	AnimDraw::instance()->setGap(5, 1);

	AnimDraw::instance()->setImage(6, ghandle[12]);
	AnimDraw::instance()->setGap(6, 1);

	AnimDraw::instance()->setImage(7, ghandle[0]);
	AnimDraw::instance()->setGap(7, 1);

	//2P
	AnimDraw::instance()->setImage(8, ghandle[8]);
	AnimDraw::instance()->setGap(8, 1);

	AnimDraw::instance()->setImage(8, ghandle[4]);
	AnimDraw::instance()->setImage(8, ghandle[5]);
	AnimDraw::instance()->setImage(8, ghandle[6]);
	AnimDraw::instance()->setImage(8, ghandle[7]);
	AnimDraw::instance()->setGap(8, 6);

	AnimDraw::instance()->setImage(9, ghandle[12]);
	AnimDraw::instance()->setImage(9, ghandle[13]);
	AnimDraw::instance()->setImage(9, ghandle[14]);
	AnimDraw::instance()->setImage(9, ghandle[15]);
	AnimDraw::instance()->setGap(9, 6);

	AnimDraw::instance()->setImage(10, ghandle[0]);
	AnimDraw::instance()->setImage(10, ghandle[1]);
	AnimDraw::instance()->setImage(10, ghandle[2]);
	AnimDraw::instance()->setImage(10, ghandle[3]);
	AnimDraw::instance()->setGap(10, 6);

	AnimDraw::instance()->setImage(11, ghandle[8]);
	AnimDraw::instance()->setImage(11, ghandle[9]);
	AnimDraw::instance()->setImage(11, ghandle[10]);
	AnimDraw::instance()->setImage(11, ghandle[11]);
	AnimDraw::instance()->setGap(11, 6);

	AnimDraw::instance()->setImage(12, ghandle[4]);
	AnimDraw::instance()->setGap(12, 1);

	AnimDraw::instance()->setImage(13, ghandle[12]);
	AnimDraw::instance()->setGap(13, 1);

	AnimDraw::instance()->setImage(14, ghandle[0]);
	AnimDraw::instance()->setGap(14, 1);
	
	//STAGE1
	LoadDivGraph("Image/map.png", 112, 14, 8, 32, 32, stagehandle);
	
	//�n��
	AnimDraw::instance()->setImage(15, stagehandle[0]);
	AnimDraw::instance()->setGap(15, 1);
	//��
	AnimDraw::instance()->setImage(16, stagehandle[45]);
	AnimDraw::instance()->setGap(16, 1);
	//��
	AnimDraw::instance()->setImage(17, stagehandle[14]);
	AnimDraw::instance()->setGap(17, 1);
	//��
	AnimDraw::instance()->setImage(18, stagehandle[47]);
	AnimDraw::instance()->setGap(18, 1);
	
	LoadDivGraph("Image/R.png", 1, 1, 1, 32, 32, stagehandle);
	//ROLL_R
	AnimDraw::instance()->setImage(19, stagehandle[0]);
	AnimDraw::instance()->setGap(19, 1);

	LoadDivGraph("Image/L.png", 1, 1, 1, 32, 32, stagehandle);
	//ROLL_L
	AnimDraw::instance()->setImage(20, stagehandle[0]);
	AnimDraw::instance()->setGap(20, 1);


	LoadDivGraph("Image/yajirusi.png", 270, 30, 9, 32, 32, yajirusihandle);
	//�E���
	AnimDraw::instance()->setImage(21, yajirusihandle[189]);
	AnimDraw::instance()->setImage(21, yajirusihandle[190]);
	AnimDraw::instance()->setImage(21, yajirusihandle[191]);
	AnimDraw::instance()->setGap(21, 6);
	//�����
	AnimDraw::instance()->setImage(22, yajirusihandle[186]);
	AnimDraw::instance()->setImage(22, yajirusihandle[187]);
	AnimDraw::instance()->setImage(22, yajirusihandle[188]);
	AnimDraw::instance()->setGap(22, 6);
	//����
	AnimDraw::instance()->setImage(23, yajirusihandle[180]);
	AnimDraw::instance()->setImage(23, yajirusihandle[181]);
	AnimDraw::instance()->setImage(23, yajirusihandle[182]);
	AnimDraw::instance()->setGap(23, 6);
	//�����
	AnimDraw::instance()->setImage(24, yajirusihandle[183]);
	AnimDraw::instance()->setImage(24, yajirusihandle[184]);
	AnimDraw::instance()->setImage(24, yajirusihandle[185]);
	AnimDraw::instance()->setGap(24, 6);

	LoadFrag = true;

}
